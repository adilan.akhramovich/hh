# It's important to parse 'vacancies' and 'resumes' sequentially and with the same 'parsed_date'


# Данный код парсит данные из https://api.hh.ru/vacancies....
# Code to parse vacancies of Almaty from hh


# import libraries
import datetime
from datetime import datetime, date
import os                   # для работы с операционной системой
import numpy as np
import pandas as pd
from psycopg2 import sql
import requests             # для работы с HTTP-запросами
from time import sleep
from sqlalchemy import create_engine
import warnings
import csv
import hashlib
import psycopg2
import random
import re
import time
import unicodedata
from bs4 import BeautifulSoup
from kzt_exchangerates import Rates
from transliterate import translit
from urllib.parse import urljoin, urlparse, parse_qs
from psycopg2 import OperationalError



warnings.filterwarnings('ignore')


print("Начинаю парсить вакансии...")



# List of proxies
proxies = {
    'socks5' : '145.239.85.58:9300',
    # 'socks5' : '46.4.96.137:1080',
    # 'socks5' : '47.91.88.100:1080',
    # 'socks5' : '45.77.56.114:30205',
    # 'socks5' : '82.196.11.105:1080',
    # 'https': '51.254.69.243:3128',
    # 'socks5' : '178.62.193.19:1080',
    # 'socks5' : '188.226.141.127:1080',
    # 'socks5' : '217.23.6.40:1080',
    # 'socks5' : '185.153.198.226:32498',
    # 'https': '81.171.24.199:3128',
    # 'socks5' : '5.189.224.84:10000',
    # 'socks5' : '108.61.175.7:31802',
    # 'https': '176.31.200.104:3128',
    # 'https': '83.77.118.53:17171',
    # 'https': '173.192.21.89:80',
    # 'https': '163.172.182.164:3128',
    # 'https': '163.172.168.124:3128',
    # 'https': '164.68.105.235:3128',
    # 'https': '5.199.171.227:3128',
    # 'https': '93.171.164.251:8080',
    # 'https': '212.112.97.27:3128',
    # 'https': '51.68.207.81:80',
    # 'https': '91.211.245.176:8080',
    # 'https': '84.201.254.47:3128',
    # 'http': '95.156.82.35:3128',
    # 'http': '185.118.141.254:808',
    # 'socks5' : '164.68.98.169:9300',
    # 'https': '217.113.122.142:3128',
    # 'https': '188.100.212.208:21129',
    # 'https': '83.77.118.53:17171',
    # 'https': '83.79.50.233:64527'
}


# List of user agents
user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPad; CPU OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPod; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A102U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-N960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-X420) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q710(FGN)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36'
]


# Select a random key-value pair from the proxies dictionary
proxy_type, proxy_address = random.choice(list(proxies.items()))
user_agent = random.choice(user_agents)


# a function to connect to PostgreSQL
def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
        print()
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


# a function to read a table from DB into a pandas DataFrame
def read_table_to_df(connection, table_name):
    query = f"SELECT * FROM {table_name}"
    df_from_DB = pd.read_sql(query, connection)
    return df_from_DB


# a function to get the 'from' salary value
def get_min_salary(salary_entry):
    if salary_entry and 'from' in salary_entry and salary_entry['from']:
        value = salary_entry['from']
        if salary_entry['currency'] != 'KZT':
            # Convert value to KZT
            if salary_entry['currency'] == 'RUR':
                conversion_rate = rates.get_exchange_rate('RUB', 'KZT')
            else:
                conversion_rate = rates.get_exchange_rate(salary_entry['currency'], 'KZT')
                
            value *= conversion_rate
        if salary_entry['gross']:
            value *= 0.8
        return value
    return None

# a function to get the 'to' salary value
def get_max_salary(salary_entry):
    if salary_entry and 'to' in salary_entry and salary_entry['to']:
        value = salary_entry['to']
        if salary_entry['currency'] != 'KZT':
            # Convert value to KZT
            if salary_entry['currency'] == 'RUR':
                conversion_rate = rates.get_exchange_rate('RUB', 'KZT')
            else:
                conversion_rate = rates.get_exchange_rate(salary_entry['currency'], 'KZT')
                
            value *= conversion_rate
        if salary_entry['gross']:
            value *= 0.8
        return value
    return None



# connect to DB
connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")

# read 'roles' table
table_name = "analytics_hh_roles"
df_roles = read_table_to_df(connection, table_name)

# read 'employers' table
table_name = "analytics_hh_employers"
df_employers = read_table_to_df(connection, table_name)

# read 'experiences' table
table_name = "analytics_hh_experiences"
df_experiences = read_table_to_df(connection, table_name)

# read 'employers' table
table_name = "analytics_hh_employ_types"
df_employ_types = read_table_to_df(connection, table_name)

# read 'skills' table
table_name = "analytics_hh_skills"
df_skills = read_table_to_df(connection, table_name)

# read 'vacancy_skills' table
table_name = "analytics_hh_skills_vacancies"
df_vacancy_skills = read_table_to_df(connection, table_name)

# read 'emp_addresses' table
table_name = "analytics_hh_emp_addresses"
df_addresses = read_table_to_df(connection, table_name)

# read 'vacancies' table
table_name = "analytics_hh_vacancies"
df_vacancies = read_table_to_df(connection, table_name)


# get today's date (data parsed date)
today = date.today()

# Format today's date as 'yyyy-mm-dd'
parsed_date = today.strftime('%Y-%m-%d') # Output: e.g., '2023-08-20'

# initialize a rates object for currency conversion
rates = Rates()


def save_database():
    global df_roles
    global df_addresses
    global df_employers
    global df_experiences
    global df_employ_types
    global df_skills
    global df_vacancy_skills
    global df_vacancies
    
    # connection.close()
    
    connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")

    #================================================================================================================#
    # insert new 'emp_addresses' rows to table
    table_name = "analytics_hh_emp_addresses"

    # Create a cursor
    cursor = connection.cursor()

    # Insert new rows into the PostgreSQL table
    for index, row in df_addresses.iterrows():

        sql_check = "SELECT id FROM " + table_name + " WHERE id = %s"
        data_check = (row['id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:

            values = (row['id'], row['street'], row['building'], row['lat'], row['lng'])  # Replace with actual column names
            cursor.execute(f"INSERT INTO {table_name} (id, street, building, lat, lng) VALUES (%s, %s, %s, %s, %s)", values)

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#
    # insert new 'employers' rows to table
    table_name = "analytics_hh_employers"

    # Create a cursor
    cursor = connection.cursor()
    
    df_employers = df_employers.dropna(axis=0)

    # Insert new rows into the PostgreSQL table
    for index, row in df_employers.iterrows():

        sql_check = "SELECT hh_id FROM " + table_name + " WHERE hh_id = %s"
        data_check = (row['hh_id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:
            values = (row['hh_id'], row['name'])  # Replace with actual column names
            cursor.execute(f"INSERT INTO {table_name} (hh_id, name) VALUES (%s, %s)", values)

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#
    # insert new 'experiences' rows to table
    table_name = "analytics_hh_experiences"

    # Create a cursor
    cursor = connection.cursor()

    # Insert new rows into the PostgreSQL table
    for index, row in df_experiences.iterrows():

        sql_check = "SELECT id FROM " + table_name + " WHERE id = %s"
        data_check = (row['id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:

            values = (row['id'], row['hh_id'], row['name_ru'], row['name_kz'])  # Replace with actual column names
            cursor.execute(f"INSERT INTO {table_name} (id, hh_id, name_ru, name_kz) VALUES (%s, %s, %s, %s)", values)

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#
    # insert new 'employers' rows to table
    table_name = "analytics_hh_employ_types"

    # Create a cursor
    cursor = connection.cursor()

    # Insert new rows into the PostgreSQL table
    for index, row in df_employ_types.iterrows():

        sql_check = "SELECT id FROM " + table_name + " WHERE id = %s"
        data_check = (row['id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:

            values = (row['id'], row['hh_id'], row['name_ru'], row['name_kz'])  # Replace with actual column names
            cursor.execute(f"INSERT INTO {table_name} (id, hh_id, name_ru, name_kz) VALUES (%s, %s, %s, %s)", values)

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#
    # insert new 'skills' rows to table
    table_name = "analytics_hh_skills"

    # Insert new rows into the PostgreSQL table
    for index, row in df_skills.iterrows():

        sql_check = "SELECT id FROM " + table_name + " WHERE id = %s"
        data_check = (row['id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:

            values = (row['id'], row['skill_ru'], row['skill_kz'])  # Replace with actual column names
            cursor.execute(f"INSERT INTO {table_name} (id, skill_ru, skill_kz) VALUES (%s, %s, %s)", values)

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#
    # insert new 'vacancies' rows to table
    table_name = "analytics_hh_vacancies"

    # Create a cursor
    cursor = connection.cursor()

    # Insert new rows into the PostgreSQL table
    for index, row in df_vacancies.iterrows():

        if pd.isna(row['id']):
            row['id'] = None

        if pd.isna(row['hh_id']):
            row['hh_id'] = None

        if pd.isna(row['name']):
            row['name'] = None

        if pd.isna(row['role_id']):
            row['role_id'] = None

        if pd.isna(row['min_salary']):
            row['min_salary'] = None

        if pd.isna(row['max_salary']):
            row['max_salary'] = None

        if pd.isna(row['address_id']):
            row['address_id'] = None

        if pd.isna(row['employer_id']):
            row['employer_id'] = None

        if pd.isna(row['experience_id']):
            row['experience_id'] = None

        if pd.isna(row['employ_type_id']):
            row['employ_type_id'] = None

        if pd.isna(row['parsed_date']):
            row['parsed_date'] = None

        sql_check = "SELECT id FROM " + table_name + " WHERE id = %s"
        data_check = (row['id'],)  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:
            #-----------------------------------------------------------------------------------------------------------
            # Дополнительная проверка на дубликаты

            sql_check = "SELECT * FROM " + table_name + " WHERE hh_id = %s AND parsed_date = %s"
            data_check = (row['hh_id'], row['parsed_date'],)  # Use the existing ID (1) here

            cursor.execute(sql_check, data_check)
            existing_record = cursor.fetchone()

            if existing_record:
                # Record with the same hh_id AND parsed_date exists; you can choose to update it or handle it as needed
                continue
            else:
                
                #-----------------------------------------------------------------------------------------------------------

                values = (row['id'], row['hh_id'], row['name'], row['role_id'], row['min_salary'], row['max_salary'], row['address_id'], row['employer_id'], row['experience_id'], row['employ_type_id'], row['parsed_date'])  # Replace with actual column names
                cursor.execute(f"INSERT INTO {table_name} (id, hh_id, name, role_id, min_salary, max_salary, address_id, employer_id, experience_id, employ_type_id, parsed_date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", values)
                

        # Commit the changes to the database
        connection.commit()

    #================================================================================================================#
    # insert new 'vacancy_skills' rows to table
    table_name = "analytics_hh_skills_vacancies"

    # Create a cursor
    cursor = connection.cursor()

    # Insert new rows into the PostgreSQL table
    for index, row in df_vacancy_skills.iterrows():

        sql_check = "SELECT * FROM " + table_name + " WHERE vacancy_id = %s AND skill_id = %s AND parsed_date = %s"
        data_check = (row['vacancy_id'], row['skill_id'], row['parsed_date'])  # Use the existing ID (1) here

        cursor.execute(sql_check, data_check)
        existing_record = cursor.fetchone()

        if existing_record:
            # Record with the same ID exists; you can choose to update it or handle it as needed
            continue
        else:
            
            sql_check = "SELECT id FROM analytics_hh_vacancies WHERE id = %s"
            data_check = (row['vacancy_id'],)  # Use the existing ID (1) here

            cursor.execute(sql_check, data_check)
            existing_record = cursor.fetchone()
            
            if existing_record:

                values = (row['vacancy_id'], row['skill_id'], row['parsed_date'])  # Replace with actual column names
                cursor.execute(f"INSERT INTO {table_name} (vacancy_id, skill_id, parsed_date) VALUES (%s, %s, %s)", values)
                
            else:
                
                continue

    # Commit the changes to the database
    connection.commit()

    #================================================================================================================#

    # Close connection
    connection.close()

    

# declare start index for main loop
role_start_index = 0

# read start index from file if file exist if the system is crashed
try:
    with open('role_index.txt', 'r') as file:
        role_start_index = int(file.read())
        
    print(role_start_index)
        
except FileNotFoundError:
    print('File not found!')

    
    
# number of pages that needs to be parsed
# number_of_vacancies = number_of_pages * per_page
number_of_pages = 20

# initiate a variable to check if the loop works
check = 1


# loop for each professional role
for hh_role_id in df_roles['hh_id'].unique()[role_start_index:]: #151
    
    with open('role_index.txt', 'w') as file:
        # Write index to the file
        file.write(str(role_start_index))
        
    role_start_index +=1
    
    # assign 'role' variable
    role_id = df_roles[df_roles['hh_id']==hh_role_id]['id'].iloc[0]

    # create an empty list
    data=[]
    
    IS_CONTINUE = False

    # iterate over each page
    for i in range(number_of_pages):
        
        if IS_CONTINUE:
            break
            
        IS_CONTINUE = False

        # hh api throws 'connection error' after over time
        # if the error appears, parser continues after a while
        while True:
            # try: 

                # pause between requests to avoid 'connection error'
#                 sleep(30)
                sleep(3)
                
                # base url that will be used
                url = 'https://api.hh.ru/vacancies'

                # 'area' 160 is Almaty, 40 is Kazakhstan
                par = {'professional_role': hh_role_id,'area':'160','per_page':'100', 'page':i}
                
                headers = {
                    "Accept": "*/*",
                    "User-Agent": user_agent
                }

                # Create the proxies dictionary
                proxies = {
                    proxy_type: proxy_address
                }
                
                r = requests.get(url, params=par, headers=headers, proxies=proxies)
                e = r.json()
                
                if int(e['pages']) == i:
                    IS_CONTINUE = True

                # parser stops after 2000 vacancies per professional_role
                # if parser stops, exit the loop
                if ('items' not in e) or len(e['items']) == 0:
                    print('check = 0, breaking')
                    check = 0
                    break

                # Convert the 'items' list in the JSON data to a DataFrame
                df_parsed = pd.DataFrame(e['items'])

                # select only the columns you need
                df = df_parsed[['id', 'name', 'salary']].copy()

                # rename a single column
                df = df.rename(columns={'id': 'hh_id'})

                # Use the safe functions to populate the min_salary and max_salary columns
                df['min_salary'] = df['salary'].apply(get_min_salary)
                df['max_salary'] = df['salary'].apply(get_max_salary)
                df.drop('salary', axis=1, inplace=True)


                # Process each item to update the addresses dataframe and assign IDs
                address_ids = []
                for item in e['items']:
                    address = item.get('address', {})

                    # If address is None or missing, skip processing this entry
                    if not address:
                        address_ids.append(None)  # or some default value like -1
                        continue

                    # Check if address exists in df_addresses
                    mask = (
                        (df_addresses['street'] == address.get('street')) &
                        (df_addresses['building'] == address.get('building')) &
                        (df_addresses['lat'] == address.get('lat')) &
                        (df_addresses['lng'] == address.get('lng'))
                    )

                    if not df_addresses[mask].empty:
                        address_id = df_addresses[mask].iloc[0]['id']
                    else:
                        # Add new address to df_addresses and get the new ID
                        address_id = len(df_addresses) + 1  # This is a simple way to generate a new ID
                        address_entry = {
                            'id': address_id,
                            'street': address.get('street'),
                            'building': address.get('building'),
                            'lat': address.get('lat'),
                            'lng': address.get('lng')
                        }
                        df_addresses = df_addresses.append(address_entry, ignore_index=True)

                    address_ids.append(address_id)

                # Add address IDs to the main DataFrame
                df['address_id'] = address_ids


                # Process each item to update the df_employers dataframe and collect employer IDs
                employer_ids = []

                for item in e['items']:
                    employer = item.get('employer', {})

                    # Check if employer ID exists in df_employers
                    mask = (df_employers['hh_id'] == employer.get('id'))

                    if not df_employers[mask].empty:
                        employer_id = employer.get('id')
                    else:
                        # Add new employer to df_employers
                        employer_entry = {
                            'hh_id': employer.get('id'),
                            'name': employer.get('name')
                        }
                        df_employers = df_employers.append(employer_entry, ignore_index=True)
                        employer_id = employer_entry['hh_id']

                    employer_ids.append(employer_id)

                # Add employer IDs to the main DataFrame
                df['employer_id'] = employer_ids


                # Process each item to update the df_experiences dataframe and collect experience IDs
                experience_ids = []

                for item in e['items']:
                    experience = item.get('experience', {})

                    # Check if experience hh_id exists in df_experiences
                    mask = (df_experiences['hh_id'] == experience.get('id'))

                    if not df_experiences[mask].empty:
                        experience_db_id = df_experiences[mask].iloc[0]['id']
                    else:
                        # Add new experience to df_experiences with incremented id
                        next_id = df_experiences['id'].max() + 1 if not df_experiences.empty else 1
                        experience_entry = {
                            'id': next_id,
                            'hh_id': experience.get('id'),
                            'name_ru': experience.get('name'),  # Assuming this example maps to name_ru
                            'name_kz': None  # Placeholder since we don't have the Kazakh name in the given data
                        }
                        df_experiences = df_experiences.append(experience_entry, ignore_index=True)
                        experience_db_id = experience_entry['id']

                    experience_ids.append(experience_db_id)

                # Add experience IDs to the main DataFrame
                df['experience_id'] = experience_ids


                # Process each employment to update the df_employ_types dataframe and collect employ_type IDs
                employ_type_ids = []

                for item in e['items']:
                    employment = item.get('employment', {})

                    # Check if employment hh_id exists in df_employ_types
                    mask = (df_employ_types['hh_id'] == employment.get('id'))

                    if not df_employ_types[mask].empty:
                        employ_type_db_id = df_employ_types[mask].iloc[0]['id']
                    else:
                        # Add new employment type to df_employ_types with incremented id
                        next_id = df_employ_types['id'].max() + 1 if not df_employ_types.empty else 1
                        employ_type_entry = {
                            'id': next_id,
                            'hh_id': employment.get('id'),
                            'name_ru': employment.get('name'),  # Assuming this example maps to name_ru
                            'name_kz': None  # Placeholder since we don't have the Kazakh name in the given data
                        }
                        df_employ_types = df_employ_types.append(employ_type_entry, ignore_index=True)
                        employ_type_db_id = employ_type_entry['id']

                    employ_type_ids.append(employ_type_db_id)

                # Add employ_type IDs to the main DataFrame
                df['employ_type_id'] = employ_type_ids

                # add role_id
                df['role_id'] = role_id

                # add parsed_date
                df['parsed_date'] = parsed_date
                
                ############################################################################################################
                # Проверка на дубликаты при повторном запуске
                
                df_vacancies = df_vacancies[['id', 'hh_id', 'name', 'role_id', 'min_salary', 'max_salary', 'address_id', 'employer_id', 'experience_id', 'employ_type_id', 'parsed_date']]

                df['hh_id'] = df['hh_id'].astype(np.int64)
                df_vacancies['hh_id'] = df_vacancies['hh_id'].astype(np.int64)

                new_df = []
                for i in range(df.shape[0]):

                    if df_vacancies[(df_vacancies['hh_id'] == df.iloc[i]['hh_id']) & (df_vacancies['parsed_date'] == datetime.strptime(df.iloc[i]['parsed_date'], "%Y-%m-%d").date())].empty:

                        new_df.append(df.loc[i].values)

                df = pd.DataFrame(new_df, columns=df.columns)
                
                if df.empty:
                    break
                
                #############################################################################################################
                
                next_id = 1
                
                if not df_vacancies.empty:
                    # Determine the next id value for df
                    next_id = df_vacancies['id'].max() + 1

                # Generate a range of id values for df
                df['id'] = range(next_id, next_id + len(df))

                # Append df to df_vacancies
                df_vacancies = df_vacancies.append(df, ignore_index=True)
            
                ##########################################################################################################################
                # Skills Code

                # hh api throws 'connection error' after over time
                # if the error appears, parser continues after a while

                for vacancy_id, hh_id in zip(df['id'], df['hh_id']):

                    key_skills = None

                    print('\t\tskill:', vacancy_id, hh_id)

                    #=========================================================================================
                    # request

                    while True:
                        try:
                            # pause between requests to avoid 'connection error'
                            # sleep(17)
                            sleep(1.7)

                            skills_url = 'https://api.hh.ru/vacancies/' + str(int(hh_id))
                            
                            headers = {
                                "Accept": "*/*",
                                "User-Agent": user_agent
                            }

                            # Create the proxies dictionary
                            proxies = {
                                proxy_type: proxy_address
                            }

                            responce = requests.get(skills_url, headers=headers, proxies=proxies)
                            key_skills = responce.json()['key_skills']

                            if len(key_skills) == 0:
                                break

                            key_skills = pd.DataFrame(key_skills)

                            if isinstance(key_skills, pd.DataFrame):
                                break

                        except:
                            # Select a random key-value pair from the proxies dictionary
                            proxy_type, proxy_address = random.choice(list(proxies.items()))
                            user_agent = random.choice(user_agents)
                            
                            # pause between requests to avoid 'connection error'
                            print('next try --->')
                            sleep(15)
                            pass

                    if len(key_skills) == 0:
                        continue

                    #=========================================================================================
                    # Add new skills to -> "df_skills"
                    
                    next_skill_id = 1
                    
                    if not df_skills.empty:

                        next_skill_id = int(df_skills.iloc[-1]['id']) + 1

                    for new_skill_name in set(key_skills['name']) - set(df_skills['skill_ru']):

                        new_row = {'id': next_skill_id, 'skill_ru': new_skill_name, 'skill_kz' : None}
                        df_skills = df_skills.append(new_row, ignore_index=True)
                        next_skill_id += 1

                    #=========================================================================================
                    # Add vacancy_id - skill_id pair to -> "df_vacancy_skills"

                    for skill_id in df_skills[df_skills['skill_ru'].isin(key_skills['name'])]['id']:

                        new_row = {'vacancy_id': vacancy_id, 'skill_id': skill_id, 'parsed_date' : parsed_date}
                        df_vacancy_skills = df_vacancy_skills.append(new_row, ignore_index=True)

                    #=========================================================================================

                ########################################################################################################################
                break

            # except:
            #     # Select a random key-value pair from the proxies dictionary
            #     proxy_type, proxy_address = random.choice(list(proxies.items()))
            #     user_agent = random.choice(user_agents)
                
            #     # pause between requests to avoid 'connection error'
            #     print('first 1500 - next try --->')
            #     sleep(15)
            #     pass
            
    save_database()

with open('role_index.txt', 'w') as file:
    # Write index to the file
    file.write("0")











# It's important to parse 'vacancies' and 'resumes' sequentially and with the same 'parsed_date'










# Данный код парсит данные из https://api.hh.ru/resumes....
# Code to parse resumes of Almaty from hh


# import libraries



print("Начинаю парсить резюме...")


# a function to convert parsed experience in a required format
def convert_to_decimal(s):
    # Extract years and months using regex
    years, months = 0, 0
    match_years = re.search(r'(\d+)\s*(?:лет|\xa0лет|год|\xa0год|year|\xa0year)', s)
    match_months = re.search(r'(\d+)\s*(?:месяц|\xa0месяц|month|\xa0month)', s)
    
    if not match_years and not match_months:
        return None
    
    if match_years:
        years = int(match_years.group(1))
        
    if not match_months:
        return years
        
    if not match_years:
        months = int(match_months.group(1))
        # Convert to decimal format
        decimal_value = (months / 12)
        return round(decimal_value, 1)  # Round to 1 decimal place
        
    if match_months:
        months = int(match_months.group(1))
        
    # Convert to decimal format
    decimal_value = years + (months / 12)
    return round(decimal_value, 1)  # Round to 1 decimal place


# a function to convert parsed salaries
def convert_to_tenge(s):
    # Extract numerical value and currency from the string
    match = re.search(r'([\d\s]+)([₸₽$⃀])', s)
    if not match:
        return None

    # Convert number string to float
    amount = float(match.group(1).replace(' ', ''))
    
    currency = match.group(2)

    # If not 'на руки' or 'in hand', divide by 0.8
    if 'на руки' not in s and 'in hand' not in s:
        amount = amount / 0.8

    # Convert other currencies to tenge
    if currency != '₸':
        rates = Rates()
        if currency == '₽':
            conversion_rate = rates.get_exchange_rate('RUB', from_kzt=True)
        elif currency == '$':
            conversion_rate = rates.get_exchange_rate('USD', from_kzt=True)
        elif currency == '⃀':
            conversion_rate = rates.get_exchange_rate('KGS', from_kzt=True)
        else:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print(s)
            return None
        
        amount = amount * conversion_rate
        
    if amount >= 5000000 or amount <= 50000:
        return None
    return round(amount)


# a function to read a table into a pandas DataFrame
def read_table_to_df(connection, table_name):
    query = f"SELECT * FROM {table_name}"
    df_from_DB = pd.read_sql(query, connection)
    return df_from_DB



# Parameters for connection to database
database = "rwh_datalake"
user = "rwh_analytics"
password = "4HPzQt2HyU@"
host = "172.30.227.205"
port = "5439"

# Connect to DB PostgreSQL
conn = psycopg2.connect(
    host=host,
    database=database,
    user=user,
    password=password,
    port=port
)
cursor = conn.cursor()

# This section is commented because 'parsed_date' was defined earlier
# get today's date (data parsed date)
# today = date.today()

# Format today's date as 'yyyy-mm-dd'
# parsed_date = today.strftime('%Y-%m-%d') # Output: e.g., '2023-08-20'

# read tables from DB
table_name = "analytics_hh_roles"
df_roles = read_table_to_df(conn, table_name)

table_name = "analytics_hh_areas"
df_areas = read_table_to_df(conn, table_name)

table_name = "analytics_hh_genders"
df_genders = read_table_to_df(conn, table_name)

table_name = "analytics_hh_education"
df_education = read_table_to_df(conn, table_name)

table_name = "analytics_hh_skills"
df_skills = read_table_to_df(conn, table_name)

table_name = "analytics_hh_skills_resumes"
df_skills_resumes = read_table_to_df(conn, table_name)

table_name = "analytics_hh_resumes"
df_resumes = read_table_to_df(conn, table_name)

# Check if the DataFrame is not empty
if not df_resumes.empty:
    df_id = df_resumes['id'].max() + 1
else:
    df_id = 1

# Максимальное количество страниц для парсинга
# hh не обрабатывает больше 2000 запросов за раз
max_pages = 100

# Базовый URL
base_url = 'https://hh.kz/resume/'


# Select a random key-value pair from the proxies dictionary
proxy_type, proxy_address = random.choice(list(proxies.items()))
user_agent = random.choice(user_agents)


# Iterate over each role
for role in df_roles['role_ru'].unique():
    print(role)
    
    # Find id corresponding to the role
    role_id = int(df_roles.loc[df_roles['role_ru'] == role, 'id'].iloc[0])
    role_hh_id = int(df_roles.loc[df_roles['role_ru'] == role, 'hh_id'].iloc[0])
    
    # The loop won't stop until all resumes are parsed
    while True:
        try: 
            # Iterate over each page
            for page in range(0, max_pages):
                # Вывод начала цикла
                # print(f"Страница {page + 1} из {max_pages} для {role}")

                # Пауза между запросами
                time_to_pause = random.randint(10, 20)
                time.sleep(time_to_pause)

                # URL for parsing
                url = f'https://hh.kz/search/resume?text=&pos=full_text&logic=normal&exp_period=all_time&area=160&professional_role={role_hh_id}&currency_code=KZT&ored_clusters=true&order_by=relevance&search_period=30&hhtmFrom=resume_search_result&page={page}'

                headers = {
                    "Accept": "*/*",
                    "User-Agent": user_agent
                }

                # Create the proxies dictionary
                proxies = {
                    proxy_type: proxy_address
                }

                req = requests.get(url, headers=headers, proxies=proxies)
                src = req.text
                soup = BeautifulSoup(src, "lxml")

                # All url to resumes from one page
                all_hrefs = soup.find_all(class_="serp-item__title")

                # If no info on a current page, iteration stops
                if not all_hrefs:
                    # print("Breaking the 'page' loop...")
                    break


                # Iterate over each resume on a page
                for item in all_hrefs:

                    # Пауза между запросами
                    time_to_pause = random.randint(2, 10)
                    time.sleep(time_to_pause)

                    # Extract hh_id 
                    item_href = item.get("href")
                    match = re.search(r'/resume/([^?]+)', item_href)
                    hh_id = match.group(1)
                    # print(hh_id)

                    # full URL of one resume
                    full_url = urljoin(base_url, hh_id)

                    # Filter the DataFrame based on the pair
                    filtered_df = df_resumes[(df_resumes['parsed_date'] == pd.Timestamp(parsed_date)) & (df_resumes['hh_id'] == hh_id)]

                    # Check if the resume was parsed today
                    if len(filtered_df) > 0:
                        # print(f'Резюме {hh_id} уже спарсено. Пропуск...')
                        continue
                        
                        
                    # Filter the DataFrame based on the pair
                    filtered_df = df_resumes[(df_resumes['parsed_date'] == parsed_date) & (df_resumes['hh_id'] == hh_id)]

                    # Check if the resume was parsed today
                    if len(filtered_df) > 0:
                        # print(f'Резюме {hh_id} уже спарсено. Пропуск...')
                        continue
                        
                        

                    else:
                        # Parse the resume
                        req = requests.get(full_url, headers=headers, proxies=proxies)
                        src = req.text
                        soup = BeautifulSoup(src, "lxml")

                        # City
                        try:
                            city = soup.find(attrs={'data-qa': "resume-personal-address"}).text.strip()
                            city = translit(city, 'ru')
                            city_id = int(df_areas[df_areas['city_ru'].str.contains(city)]['hh_id'].iloc[0])
                        except:
                            city = None

                        # Gender
                        try:
                            gender = soup.find(attrs={'data-qa': "resume-personal-gender"}).text.strip()
                            if gender in df_genders['gender_ru'].values:
                                gender_id = int(df_genders.loc[df_genders['gender_ru'] == gender, 'id'].iloc[0])
                            elif gender in df_genders['gender_en'].values:
                                gender_id = int(df_genders.loc[df_genders['gender_en'] == gender, 'id'].iloc[0])
                            elif gender in df_genders['gender_kz'].values:
                                    gender_id = int(df_genders.loc[df_genders['gender_kz'] == gender, 'id'].iloc[0])
                            else:
                                print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
                                print('ID NOT ASSIGNED...............')
                        except:
                            gender_id = 3

                        # Job title
                        item_text = item.text

                        # Age
                        try:
                            age = soup.find(attrs={'data-qa': "resume-personal-age"}).text.strip()
                            match = re.search(r'(\d+)', age)
                            age = match.group(1)
                        except:
                            age = None

                        # Salary
                        try:
                            salary_elem = soup.find(attrs={'class': "resume-block__salary"})
                            salary_level = unicodedata.normalize("NFKD", salary_elem.text).strip()
                            salary = convert_to_tenge(salary_level)
                        except:
                            salary = None

                        # Total experience
                        try:
                            experience_level = soup.find(attrs={'class': "resume-block__title-text resume-block__title-text_sub"}).text.strip()
                            experience = convert_to_decimal(experience_level)
                        except:
                            experience = None

                        # Last experience
                        try:
                            last_experience_level = soup.find(attrs={'class': 'bloko-text bloko-text_tertiary'}).text.strip()
                            last_experience = convert_to_decimal(last_experience_level)
                        except:
                            last_experience = None

                        # Education
                        try:
                            # Find the container first
                            education_container = soup.find(attrs={'data-qa': "resume-block-education"})

                            # Now find the specific span within this container
                            education_level = education_container.find('span', class_='resume-block__title-text_sub').text.strip()

                            # Find index from 'analytics_df_education'
                            if education_level in df_education['education_ru'].values:
                                education_id = int(df_education.loc[df_education['education_ru'] == education_level, 'id'].iloc[0])
                            else:
                                education_id = df_education['id'].max() + 1

                                # Append values to the dataframe
                                df_education = df_education.append({'id': education_id, 'education_ru': education_level}, ignore_index=True)

                                # Update 'analytics_hh_educaton' table in DB
                                cursor.execute(
                                '''INSERT INTO analytics_hh_education(id, education_ru)
                                VALUES (%s, %s)''',
                                (education_id, education_level)
                                )
                                conn.commit()
                        except:
                            education_id = 4            


                        # Вставка данных в базу данных PostgreSQL
                        # Update 'analytics_hh_resumes' table in DB
                        cursor.execute(
                            '''INSERT INTO analytics_hh_resumes(id, city_id, gender_id, job_title, role_id, age, salary, total_experience, last_experience, education_id, parsed_date, hh_id)
                            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)''',
                            (df_id, city_id, gender_id, item_text, role_id, age, salary, experience, last_experience, education_id, parsed_date, hh_id)
                        )
                        conn.commit()
                        
                        # Update 'df_resumes' table
                        # Values to insert
                        new_row = {
                            'id': df_id, 
                            'city_id': city_id, 
                            'gender_id': gender_id, 
                            'job_title': item_text, 
                            'role_id': role_id, 
                            'age': age, 
                            'salary': salary, 
                            'total_experience': experience, 
                            'last_experience': last_experience, 
                            'education_id': education_id, 
                            'parsed_date': parsed_date, 
                            'hh_id': hh_id
                        }
                        df_resumes = df_resumes.append(new_row, ignore_index=True)

                        # Skills
                        if df_id not in df_skills_resumes['resume_id'].values:
                            try:
                                skills_list = soup.find(attrs={'class': "bloko-tag-list"})
                                skills_array = [skill.text for skill in skills_list] if skills_list else []    

                                # Iterate over skills and find the index
                                for skill in skills_array:
                                    # Check if the skill string is not empty
                                    if skill:

                                        # If the skill is found in the dataframe
                                        if skill in df_skills['skill_ru'].values:
                                            skill_id = int(df_skills.loc[df_skills['skill_ru'] == skill, 'id'].iloc[0])

                                        # If the skill is not found in the dataframe
                                        else:
                                            # Append the skill to the dataframe
                                            skill_id = df_skills['id'].max() + 1
                                            df_skills = df_skills.append({'id': skill_id, 'skill_ru': skill}, ignore_index=True)

                                            # Update 'analytics_hh_educaton' table in DB
                                            cursor.execute(
                                            '''INSERT INTO analytics_hh_skills(id, skill_ru)
                                            VALUES (%s, %s)''',
                                            (skill_id, skill)
                                            )
                                            conn.commit()

                                        # Update 'analytics_hh_skills_resumes' table in DB
                                        cursor.execute(
                                        '''INSERT INTO analytics_hh_skills_resumes(resume_id, skill_id)
                                        VALUES (%s, %s)''',
                                        (df_id, skill_id)
                                        )
                                        conn.commit()
                            except:
                                pass

                        # Update index id
                        df_id += 1

            break
            
        except:
            # Reselect a random key-value pair from the proxies dictionary
            proxy_type, proxy_address = random.choice(list(proxies.items()))
            user_agent = random.choice(user_agents)
            
            print('Вышла ошибка, перезапускаю цикл...')

print("Парсинг завершен!")

# Завершение транзакции и закрытие соединения с PostgreSQL
cursor.close()
conn.close()