# import libraries
import json
import os
import time
import pandas as pd
import psycopg2
import random
import requests
import sqlalchemy as sql
import warnings
from bs4 import BeautifulSoup
from datetime import date
from datetime import datetime
from io import StringIO
from kzt_exchangerates import Rates
from time import sleep
from tqdm import tqdm 
from psycopg2 import OperationalError

warnings.filterwarnings("ignore", message="The default value of regex will change from True to False in a future version.")


# a function to connect to PostgreSQL
def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg2.connect(
            database=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


# a function to append data from DataFrame to the existing table
def append_data_to_postgres(df, tablename, conn):
    """
    Append a DataFrame to a PostgreSQL table using psycopg2.

    Parameters:
    - df: Pandas DataFrame
    - tablename: PostgreSQL table name
    - connection_string: PostgreSQL connection string
    """
    # Establish connection
    cursor = conn.cursor()

    # Use StringIO to convert dataframe into a file object so we can use copy_from
    output = StringIO()
    df.to_csv(output, sep='\t', header=False, index=False)
    output.seek(0)

    # Insert data into table
    cursor.copy_from(output, tablename, columns=tuple(df.columns), null="")  # null values become ''
    conn.commit()

    # Close connection
    cursor.close()
    conn.close()


# a function to read a table into a pandas DataFrame
def read_table_to_df(connection, table_name):
    query = f"SELECT * FROM {table_name}"
    df_from_DB = pd.read_sql(query, connection)
    return df_from_DB



# connect to DB
connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")

# read tables from DB
table_name = "analytics_hh_areas"
df_areas = read_table_to_df(connection, table_name)

table_name = "analytics_hh_roles"
df_roles = read_table_to_df(connection, table_name)


# get today's date (data parsed date)
today = date.today()

# Format today's date as 'yyyy-mm-dd'
parsed_date = today.strftime('%Y-%m-%d') # Output: e.g., '2023-08-20'

# Format today's date as 'dd.mm.yyyy'
parsed_date2 = today.strftime('%d.%m.%Y') # Output: e.g., '20.08.2023'


# get exchange rate from RUBLE to KZ TENGE
rates = Rates()
exchange_rate = rates.get_exchange_rate("RUB", from_kzt=True, date=parsed_date2)

# create an empty df for all cities
df_all = pd.DataFrame()


# create an empty df
df = pd.DataFrame()


# List of proxies
proxies = {
    'socks5' : '145.239.85.58:9300',
    'socks5' : '46.4.96.137:1080',
    'socks5' : '47.91.88.100:1080',
    'socks5' : '45.77.56.114:30205',
    'socks5' : '82.196.11.105:1080',
    'https': '51.254.69.243:3128',
    'socks5' : '178.62.193.19:1080',
    'socks5' : '188.226.141.127:1080',
    'socks5' : '217.23.6.40:1080',
    'socks5' : '185.153.198.226:32498',
    'https': '81.171.24.199:3128',
    'socks5' : '5.189.224.84:10000',
    'socks5' : '108.61.175.7:31802',
    'https': '176.31.200.104:3128',
    'https': '83.77.118.53:17171',
    'https': '173.192.21.89:80',
    'https': '163.172.182.164:3128',
    'https': '163.172.168.124:3128',
    'https': '164.68.105.235:3128',
    'https': '5.199.171.227:3128',
    'https': '93.171.164.251:8080',
    'https': '212.112.97.27:3128',
    'https': '51.68.207.81:80',
    'https': '91.211.245.176:8080',
    'https': '84.201.254.47:3128',
    'http': '95.156.82.35:3128',
    'http': '185.118.141.254:808',
    'socks5' : '164.68.98.169:9300',
    'https': '217.113.122.142:3128',
    'https': '188.100.212.208:21129',
    'https': '83.77.118.53:17171',
    'https': '83.79.50.233:64527'
}


# List of user agents
user_agents = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 13_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/16.1 Safari/605.1.15',
    'Mozilla/5.0 (iPhone; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPad; CPU OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (iPod; CPU iPhone OS 17_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/117.0.5938.117 Mobile/15E148 Safari/604.1',
    'Mozilla/5.0 (Linux; Android 10) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A205U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-A102U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-G960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; SM-N960U) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q720) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-X420) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36',
    'Mozilla/5.0 (Linux; Android 10; LM-Q710(FGN)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/117.0.5938.153 Mobile Safari/537.36'
]


# Select a random key-value pair from the proxies dictionary
proxy_type, proxy_address = random.choice(list(proxies.items()))
user_agent = random.choice(user_agents)




# iterate for each area
for j in range(len(df_areas)):  
    city_id = df_areas['hh_id'][j]  
    city_name = df_areas['city_ru'][j]
    
    for i in range(len(df_roles)): 
        # hh api throws 'connection error' after over time
        # if the error appears, parser continues after a while
        while True:
            try:                
                role_id = df_roles['id'][i]
                role_hh_id = df_roles['hh_id'][i]
                role = df_roles['role_ru'][i]  
                
                # Пауза между запросами
                time_to_pause = random.randint(5, 10)
                time.sleep(time_to_pause)
                
                base_url = "https://hh.ru/search/vacancy?"
                params = {
                    "text": '',
                    "salary": "",
                    "area": city_id,
                    "professional_role": role_hh_id,
                    "ored_clusters": "true"
                }
                query_string = "&".join([f"{key}={value}" for key, value in params.items()])
                url = base_url + query_string
                headers = {'User-Agent': user_agent}
                
                # Create the proxies dictionary
                proxies = {
                    proxy_type: proxy_address
                }

                # ceate a session
                session = requests.Session()
                r = session.get(url, timeout=30, headers=headers, proxies=proxies)
                soup = BeautifulSoup(r.content, 'html.parser')
                dates_1 = soup.find_all("li", class_="novafilters-list__item")
                dates_2 = soup.find_all("span", class_="bloko-text bloko-text_tertiary")
                data = []
                for date_1, date_2 in zip(dates_1, dates_2):
                    date_1_text = date_1.text.strip()
                    data.append((date_1_text))

                df_temp = pd.DataFrame(data, columns=['Fil'])
                df_temp["Value"] = df_temp["Fil"].str.extract(r'(\d+\s*\d*)$')
                df_temp["Value"] = df_temp["Value"].str.replace(' ', '').astype(str)
                df_temp['Fil'] = df_temp['Fil'].str.replace(r'\d+\s*\d*$', '').str.strip()
                df_temp['role_id'] = [role_id] * len(df_temp)
                df = pd.concat([df, df_temp], axis=0)
                break
                
            except:
                # Reselect a random key-value pair from the proxies dictionary
                proxy_type, proxy_address = random.choice(list(proxies.items()))
                user_agent = random.choice(user_agents)

                print('Вышла ошибка, перезапускаю цикл...')
                pass
                
    df = df.pivot_table(index='role_id', columns='Fil',values = "Value", aggfunc=lambda x: x.max())

    # add categories to df (https://api.hh.ru/professional_roles)
    # df = df_roles.join(df.set_index('role_ru'), on='role_ru')
    
    df = df.merge(df_roles, left_on='role_id', right_on='id', how='inner')

    # rename columns (containing salaries) to int
    int_cols = []
    count = 0
    for col in df.columns:
        if count == 0:
            if city_name in col:
                df = df.rename(columns={col: 'vacancy_num'})
                count = 1

        if 'руб.' in col:
            col_salary = int(col[2:-4].replace(" ", ""))
            df = df.rename(columns={col: col_salary})
            int_cols.append(col_salary)

        elif '₽' in col:
            col_salary = int(col[2:-1].replace(" ", ""))
            df = df.rename(columns={col: col_salary})
            int_cols.append(col_salary)


    # sort columns containing salaries
    int_cols.sort(reverse=True)
    sorted_df = df[int_cols]

    sorted_df['salary'] = 0
    sorted_df['vacancy_num_with_salary'] = 0

    # calculate median salary and number of vacancies with salary
    if len(int_cols) != 0:
        for col in sorted_df.columns:
            if type(col) == int:
                if sorted_df[col].dtype == 'O':
                    sorted_df[col].fillna('0', inplace=True)
                    sorted_df.loc[:, col] = sorted_df[col].str.replace('\u202f', '')

                else:
                    sorted_df[col].fillna(0, inplace=True)

                sorted_df.loc[:, col] = sorted_df[col].astype(int)

                sorted_df.loc[sorted_df[col]!=0, 'salary'] += (sorted_df[col]-sorted_df['vacancy_num_with_salary']) * col
                sorted_df.loc[sorted_df[col]!=0, 'vacancy_num_with_salary'] = sorted_df[col]

        sorted_df.loc[sorted_df['vacancy_num_with_salary'] != 0, 'salary'] /= sorted_df['vacancy_num_with_salary']


        # change 'salary' from RUBLE to KZ TENGE
        sorted_df['salary'] = sorted_df['salary'] * exchange_rate
        sorted_df['salary'] = round(sorted_df['salary'] / 100) * 100
        sorted_df.loc[:, 'salary'] = sorted_df['salary'].astype(int)

    # rename column
    df = df.rename(columns={'id': 'role_id'})

    # list of columns required for analysis
    required_cols = ['role_id', 'vacancy_num', 'parsed_date', 'city_id',
                     'Временная работа', 'Неполный день', 'От 4 часов в день', 'По вечерам', 'Разовое задание', 'По выходным',
                     'Не\xa0требуется или не\xa0указано', 'Высшее', 'Среднее профессиональное',
                     'От\xa01\xa0года до\xa03\xa0лет', 'От\xa03\xa0до\xa06\xa0лет', 'Нет опыта', 'Более 6\xa0лет',
                     'Полная занятость', 'Частичная занятость', 'Стажировка', 'Проектная работа', 'Волонтерство',
                     'Полный день', 'Удаленная работа', 'Гибкий график', 'Сменный график', 'Вахтовый метод',
                     'salary', 'vacancy_num_with_salary'
                    ]

    # keep only required columns
    df = df.reindex(columns=required_cols)

    # assign values to columns
    df['city_id'] = city_id
    df['parsed_date'] = parsed_date
    df['salary'] = sorted_df['salary']
    df['vacancy_num_with_salary'] = sorted_df['vacancy_num_with_salary']

    # convert columns (except 'prof_role') to int
    for col in df.columns:
        if col not in ['role_id', 'city_id', 'parsed_date']:                            
            if df[col].dtype == 'object':
                df[col] = df[col].replace('nan', '0').fillna('0')
                df.loc[:, col] = df[col].str.replace('\u202f', '')

            else:
                df[col].fillna(0, inplace=True)

            df.loc[:, col] = df[col].astype(int)

    # drop rows where 'vacancy_num' is 0
    df = df.drop(df[df['vacancy_num'] == 0].index)

    # append to the main df
    df_all = pd.concat([df_all, df])


# drop rows where vacancy_num is Nan
df_all = df_all.dropna(subset=['vacancy_num'])


# connect to DB
connection = create_connection("rwh_datalake", "rwh_analytics", "4HPzQt2HyU@", "172.30.227.205", "5439")

# append data from DataFrame to the existing table
append_data_to_postgres(df_all, 'analytics_hh_all_cities', connection)

print("Парсинг завершен!")